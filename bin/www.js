/**
 * JSON as default
 * uncomment line 6 to work with MySQL
 */
let app = require('../app');
// app = require('../app_sql');

// set port
const port = process.env.PORT || '3001';
app.set('port', port);

// listen to server
app.listen(port, () => {
  console.log(`App listening on port ${port}!`);
});

