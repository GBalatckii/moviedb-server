/**
 * Delete all tables, test input from client, film by title or an actor by name
 * Arguments info: './arguments.json'
 * SQL user info: '../.env'
 */
const mysql = require('mysql');
const argumentParser = require('node-argument-parser');
const commands = require('./sql_commands');

// get SQL commands
const sqlCommands = commands.deleteCommands;

// take all arguments
const argv = argumentParser.parse('./arguments.json', process);

// set process.env from local file to use MySQL
require('dotenv').config();

// create sql pool
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_FILM
});

/**
 * Delete from SQL
 * @param {string} sql - sql query command
 * @param {string} arg - film title or actor name or empty string
 * @param {string} name - info for console.log
 */
function deleteFromDb({ sql, arg = '', name }) {
  return new Promise((resolve, reject) => {
    pool.query(sql, arg, (err, result) => {
      if (err) reject(err);
      resolve(result);
    })})
      .then((result) => {
        if (result.affectedRows > 0 || name === 'Tables') {
          console.log(`${name} was/were successfully deleted`);
        } else {
          console.log(`${name} was/were not found`);
        }
      })
      .catch(err => { throw new Error(`${err.message}`);});
}

/**
 * Check parameters and then delete
 * @param {boolean} all - drop all tables
 * @param {boolean} test - delete test input
 * @param {(string|null)} film - title for film
 * @param {(string|null)} actor - name of actor
 * @returns {Promise.<*[]>}
 */
function deleteMain({ all, test, film, actor }) {
  if (all) {
    return deleteFromDb({ sql: sqlCommands.deleteAll, name: 'Tables' });
  }
  return Promise.all([
    test ? deleteFromDb({ sql: sqlCommands.deleteTest, name: 'TestInput' }) : true,
    film ? deleteFromDb({ sql: sqlCommands.deleteFilm, arg: film, name: film }) : true,
    actor ? deleteFromDb({ sql: sqlCommands.deleteActor, arg: actor, name: actor }) : true
  ]);
}

// If right argument: create connection => delete => close connection
// If --help: help message from 'node-argument-parser'
// Else: Info message
if (argv.all || argv.test || argv.film || argv.actor) {
  new Promise((resolve, reject) => {
    console.log('\n<--- Start deleting --->');

    pool.getConnection((error) => {
      if (error) reject(error);
      deleteMain(argv)
        .then(() => resolve())
        .catch((err) => reject(err))
    });
  })
    .catch((err) => console.log(`${err.message}`))
    .then(() => {
      console.log('<--- DONE! --->\n');
      pool.end();
    });
} else if (!argv.help) {
  console.log('\nNo arguments found. Type --help for more information\n');
}
