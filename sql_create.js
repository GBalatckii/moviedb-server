/**
 * Create SQL tables: movies, actors and 'mov_act' with movie-actor
 * @type {"fs"} to work with JSON file
 */
const fs = require('fs');
const mysql = require('mysql');
const commands = require('./sql_commands');

// get SQL commands
const sqlCommands = commands.createCommands;

// set process.env from local file to use MySQL
require('dotenv').config();

// connect to mySQL
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_FILM
});

// read the JSON and save the film list
const filmsJSON = fs.readFileSync('./public/json/data.json');
const films = JSON.parse(filmsJSON);

// Filter the JSON film list from undefined
const sortFilms = films.movies.filter(val =>
  (val.title &&
    val.year &&
    val.ratings &&
    val.ratings.critics_score && val.ratings.critics_score > 0 &&
    val.ratings.audience_score && val.ratings.audience_score > 0 &&
    val.posters && val.posters.original &&
    val.abridged_cast && val.abridged_cast.length >= 5 &&
    val.alternate_ids && val.alternate_ids.imdb));

/**
 * Create a table
 * @param {string} sql - sql query command
 */
function createTable(sql) {
  // take the table name from 'CREATE TABLE name ...'
  const name = sql.split(' ')[2];
  return new Promise((resolve, reject) => {
    pool.query(sql, (err) => {
      if (err) reject(err);
      resolve();
    })
  })
    .then(() => console.log(`Table ${name} was successfully created`))
    .catch(err => {throw new Error(`Error creating ${name}: ${err.message}`)});
}

/**
 * Insert values into table
 * @param {string} sql - sql query command
 * @param {Array[]} values - list of inserted values [[value1], [value2], ... ]
 */
function insertTable(sql, values) {
  // take the table name from 'INSERT INTO name ...'
  const name = sql.split(' ')[2];
  return new Promise((resolve, reject) => {
    pool.query(sql, [values], (err, result) => {
      if (err) reject(err);
      resolve(result);
    })
  })
    .then(result => console.log(`Values were inserted in ${name}: ${result.affectedRows}`))
    .catch(err => {throw new Error(`Error inserting in ${name}: ${err.message}`)});
}

/**
 * Get all actor names from sorted list of films
 * @returns {Array[]} like [[name1], [name2], ... ]
 */
function getActors() {
  const values = new Set();

  // Push to SQL Array
  sortFilms.forEach((film) => {
    film.abridged_cast.forEach((actor) => {
      values.add(actor.name);
    });
  });

  return Array.from(values).map(val => [val]);
}

/**
 * Get all films from sorted list of films
 * @returns {Array[]} with all films like [[title1, year1, ... ], [title2, year2, ...], ... ]
 */
function getFilms() {
  const values = [];

  // Push to SQL Array
  sortFilms.forEach((film) => {
    values.push([film.title, film.year, film.ratings.critics_score,
      film.ratings.audience_score, film.posters.original, film.synopsis, film.alternate_ids.imdb]);
  });

  return values;
}

/**
 * Search the value in array and return the key(id)
 * @param {string} search - the value to search (film title or actor name)
 * @param {Array} arr - array with Objects [{key1: value1}, {key2: value2}, ...]
 * @returns {number} the id
 */
function findId(search, arr) {
  let found = -1;
  arr.forEach((val) => {
    if (Object.values(val).indexOf(search) > -1) { found = Object.keys(val); }
  });
  return found;
}

/**
 * Insert values in mov_act table
 * @returns {Promise.<>}
 */
function insertMovAct() {
  // values: start Array[film.title , actor.name]
  // idValues: end Array[film.id, actor.id]
  const values = [];
  let idValues = [];

  // Push films and actors in array
  sortFilms.forEach((val) => {
    val.abridged_cast.forEach((value) => {
      values.push([val.title, value.name]);
    });
  }, []);

  // filmTable: Array[title, id]
  // actorTable: Array[actor, id]
  const filmTable = [];
  const actorTable = [];

  // Get the table with films id's => push in filmTable like {id: title}
  // Get the table with actors id's => push in actorTable like {id: name}
  return Promise.all([
    new Promise ((resolve, reject) => {
      pool.query(sqlCommands.getAllMovies, (err, result) => {
        if (err) reject(err);
        resolve(result);
      })
    })
      .then((result) => {
        result.forEach((val) => {
          filmTable.push({ [parseInt(val.id, 10)]: val.title });
        });
      })
      .catch((err) => {throw err;}),
    new Promise ((resolve, reject) => {
      pool.query(sqlCommands.getAllActors, (err, result) => {
        if (err) reject(err);
        resolve(result);
      })
    })
      .then((result) => {
        result.forEach((val) => {
          actorTable.push({ [parseInt(val.id, 10)]: val.name });
        });
      })
      .catch((err) => {throw err;})
  ])
  // Push new values to array
    .then(() => {
      idValues = values.map((val) => {
        const title = findId(val[0], filmTable);
        const actor = findId(val[1], actorTable);
        return [title, actor];
      });
    })
    // insert new values in mov_act
    .then(() => insertTable(sqlCommands.insertMovAct, idValues))
    .catch((err) => {throw err;});
}

/**
 * Create connection => movies table => actors table =>
 * insert into movies => insert into actors =>
 * create foreign table mov_act => insert values =>
 * error? => "Done" => close connection
 */
new Promise((resolve, reject) => {
  console.log('\n<--- Start creating tables --->');
  pool.getConnection((error) => {
    if (error) reject(error);
    createTable(sqlCommands.createMovies)
      .then(() => createTable(sqlCommands.createActors))
      .then(() => insertTable(sqlCommands.insertMovies, getFilms()))
      .then(() => insertTable(sqlCommands.insertActors, getActors()))
      .then(() => createTable(sqlCommands.createMovAct))
      .then(() => insertMovAct())
      .then(() => resolve())
      .catch((err) => reject(err))
  });
})
  .catch((err) => console.log(`${err.message}`))
  .then(() => {
    console.log('<--- DONE! --->\n');
    pool.end();
  });
