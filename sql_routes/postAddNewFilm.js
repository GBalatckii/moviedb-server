const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const commands = require('../sql_commands');

// get SQL commands
const sqlCommands = commands.serverCommands;

// connect to mySQL
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_FILM
});

/* POST add new film */
router.post('/', function(req, res) {
  new Promise((resolve, reject) => {
    pool.getConnection((error, connection) => {
      // error with connection
      if (error) {
        connection.release();
        reject(error);
      }

      pool.query(sqlCommands.getFilmByTitleAndYear, [req.body.title, req.body.year], (err, result) => {
        if (error) {
          connection.release();
          reject(error);
        } else if (result && result.length > 0) {
          connection.release();
          reject(new Error('Already exist')); }
        else {
          // first insert into movies and actors
          Promise.all([
            insertIntoTable(
              sqlCommands.insertIntoMovies,
              [[
                req.body.title,
                req.body.year,
                req.body.ratings.critics_score,
                req.body.ratings.audience_score,
                req.body.posters.original,
                req.body.synopsis,
                req.body.alternate_ids.imdb
              ]]
            ),
            insertIntoTable(
              sqlCommands.insertIntoActors,
              [[
                [req.body.abridged_cast[0].name],
                [req.body.abridged_cast[1].name],
                [req.body.abridged_cast[2].name],
                [req.body.abridged_cast[3].name],
                [req.body.abridged_cast[4].name]
              ]]
            )
          ])
            // then as foreign keys into mov_act
            .then(() => insertIntoTable(
              sqlCommands.insertIntoMovAct,
              [
                req.body.title,
                [
                  req.body.abridged_cast[0].name,
                  req.body.abridged_cast[1].name,
                  req.body.abridged_cast[2].name,
                  req.body.abridged_cast[3].name,
                  req.body.abridged_cast[4].name
                ]
              ]
            ))
            .then(() => {
              connection.release();
              console.log('SUCCESS');
              res.status(200).json(req.body);
            })
            .catch((err) => {
              connection.release();
              reject(err);
            });
        }
      });
    });
  })
    .catch((err) => {
      console.log(`${err.message}`);
      res.status(400).send(err.message);
    });
});

/**
 * Insert values into table
 * @param {string} sql - sql command
 * @param {Array} values - values to insert
 * @returns {Promise}
 */
function insertIntoTable(sql, values) {
  return new Promise((resolve, reject) => {
    // Take the name from SQL: 'INSERT INTO ... '
    const index = sql.indexOf('(');
    const name = sql.slice(0,index);
    console.log(name);

    pool.query(sql, values, (err) => {
      if (err) { reject(err); }
      else { resolve();}
    });
  })
}

module.exports = router;
