const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const commands = require('../sql_commands');

// get SQL commands
const sqlCommands = commands.serverCommands;

// connect to mySQL
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_FILM
});

/* GET all films */
router.get('/', function(req, res) {
  new Promise((resolve, reject) => {
    pool.getConnection((error, connection) => {
      // error with connection
      if (error) {
        connection.release();
        reject(error);
      }

      pool.query(sqlCommands.getMoviesAndActors, (err, result) => {
        if (error) {
          connection.release();
          reject(error);
        } else if (!result || result.length === 0) {
          connection.release();
          reject(new Error('Nothing found')); }
        else {
          // save all films in this object (as in JSON)
          const films = {
            movies: [],
          };

          // sort the Object to just one film with five actors
          const filmMap = new Map();

          // sort each movie as in JSON to not change the logic on client side
          result.forEach((val) => {
            let match = true;

            // check if we already have this film
            if (!filmMap.has(val.title)) {
              // set as new film
              filmMap.set(val.title, 'new film');
              match = false;
            }

            // If no match: add new film to our list
            // If match: find index of this film in our list and push new actor
            if (!match) {
              films.movies.push({
                title: val.title,
                year: val.year,
                ratings: {
                  critics_score: val.critics,
                  audience_score: val.users,
                },
                synopsis: val.synopsis,
                posters: {
                  original: val.poster,
                },
                abridged_cast: [
                  {name: val.name}
                ],
                alternate_ids: {
                  imdb: val.link,
                }
              });
            } else {
              const index = films.movies.findIndex(value => value.title === val.title);
              films.movies[index].abridged_cast.push({name: val.name});
            }
          });

          console.log(`Films sent: ${films.movies.length}`);
          res.status(200).json(films);
          connection.release();
          resolve();
        }
      });
    });
  })
    .catch((err) => {
      console.log(`${err.message}`);
      res.status(400).send(err.message);
    });
});

module.exports = router;
