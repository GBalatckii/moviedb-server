const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const commands = require('../sql_commands');

// get SQL commands
const sqlCommands = commands.serverCommands;

// connect to mySQL
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_FILM
});

/* GET all actors */
router.get('/(:actor)?', function(req, res) {
  const match = `%${(req.params.actor || '').toLowerCase()}%`;

  new Promise((resolve, reject) => {
    pool.getConnection((error, connection) => {
      // error with connection
      if (error) {
        connection.release();
        reject(error);
      }

      pool.query(sqlCommands.getActors, match, (err, result) => {
        if (error) {
          connection.release();
          reject(error);
        } else if (!result || result.length === 0) {
          connection.release();
          reject(new Error('Nothing found')); }
        else {
          console.log(`Actors sent: ${result.length}`);
          res.status(200).json(result.map(val => val.name));
          connection.release();
          resolve();
        }
      });
    });
  })
    .catch((err) => {
      console.log(`${err.message}`);
      res.status(400).send(err.message);
    });
});

module.exports = router;
