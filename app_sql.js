const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

// set process.env from local file to use MySQL
require('dotenv').config();

// Routes
const getAllFilms = require('./sql_routes/getAllFilms');
const getAllActors = require('./sql_routes/getAllActors');
const getAllActorFilms = require('./sql_routes/getAllActorFilms');
const postAddNewFilms = require('./sql_routes/postAddNewFilm');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use((req, res, next) => {
  if (req.method !== 'OPTIONS') {
    console.log('\n<--- %s %s --->', req.method.toUpperCase(), req.path);
  }
  next();
});

// choose the route
app.use('/loadfilms', getAllFilms);
app.use('/loadactors', getAllActors);
app.use('/loadactorfilms', getAllActorFilms);
app.use('/addnewfilm', postAddNewFilms);

module.exports = app;
