// commands for sql_delete.js
const deleteCommands = {
  deleteAll: 'DROP TABLE mov_act, movies, actors;',
  deleteTest: 'DELETE m.*, a.* FROM movies m, actors a WHERE m.title = "FilmTitle" AND a.name LIKE "%first%";',
  deleteFilm: 'DELETE FROM movies WHERE title= ?;',
  deleteActor: 'DELETE FROM actors WHERE name= ?;',
};

// commands for sql_create.js
const createCommands = {
  createActors: `CREATE TABLE actors (
                id INT AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(255) );`,
  createMovies: `CREATE TABLE movies (
                id INT AUTO_INCREMENT PRIMARY KEY,
                title VARCHAR(255),
                year INT(4) UNSIGNED,
                critics INT(3) UNSIGNED,
                users INT(3) UNSIGNED,
                poster VARCHAR(255),
                synopsis TEXT(1000),
                link INT(7) ZEROFILL UNSIGNED );`,
  createMovAct: `CREATE TABLE mov_act (
                id INT AUTO_INCREMENT PRIMARY KEY,
                mov_id INT,
                act_id INT,
                FOREIGN KEY (mov_id)
                REFERENCES movies(id)
                ON DELETE CASCADE,
                FOREIGN KEY (act_id)
                REFERENCES actors(id)
                ON DELETE CASCADE );`,
  insertMovies: 'INSERT INTO movies (title, year, critics, users, poster, synopsis, link) VALUES ? ;',
  insertActors: 'INSERT INTO actors (name) VALUES ? ;',
  insertMovAct: 'INSERT INTO mov_act (mov_id, act_id) VALUES ? ;',
  getAllMovies: 'SELECT m.id, m.title FROM movies m;',
  getAllActors: 'SELECT a.id, a.name FROM actors a;'
};

// commands for app_sql.js
const serverCommands = {
  getMoviesAndActors: `SELECT m.*, a.name 
                      FROM movies m 
                      JOIN mov_act ma ON m.id=ma.mov_id 
                      JOIN actors a ON ma.act_id=a.id;`,
  getFilmByTitleAndYear: 'SELECT title, year FROM movies WHERE title= ? AND year= ?;',
  insertIntoMovies: 'INSERT INTO movies (title, year, critics, users, poster, synopsis, link) VALUES ( ? );',
  insertIntoActors: 'INSERT INTO actors (name) VALUES ? ;',
  insertIntoMovAct: `INSERT INTO mov_act (mov_id, act_id) 
                    SELECT m.id, a.id 
                    FROM movies m 
                    JOIN actors a 
                    WHERE m.title = ? AND a.name IN( ? );`,
  getActors: 'SELECT name FROM actors WHERE name LIKE ? ;',
  getFilms: `SELECT m.title, m.link 
            FROM movies m 
            WHERE m.id IN 
              (SELECT ma.mov_id 
              FROM mov_act ma 
              WHERE ma.act_id = 
                (SELECT a.id 
                FROM actors a 
                WHERE a.name = ? ))
            ORDER BY m.title ;`
};

module.exports = {
  deleteCommands,
  createCommands,
  serverCommands
};
