const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

/**
 * Routes
 */
const getAllFilms = require('./routes/getAllFilms');
const getAllActors = require('./routes/getAllActors');
const getAllActorFilms = require('./routes/getAllActorFilms');
const postAddNewFilms = require('./routes/postAddNewFilm');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use((req, res, next) => {
  if (req.method !== 'OPTIONS') {
    console.log('\n<--- %s %s --->', req.method.toUpperCase(), req.path);
  }
  next();
});

// choose the route
app.use('/loadfilms', getAllFilms);
app.use('/loadactors', getAllActors);
app.use('/loadactorfilms', getAllActorFilms);
app.use('/addnewfilm', postAddNewFilms);

module.exports = app;
