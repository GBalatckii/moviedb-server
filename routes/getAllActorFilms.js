const express = require('express');
const router = express.Router();
const fs = require('fs');

/* GET films with actor */
router.get('/(:actor)?', function(req, res) {
  const filmsJSON = fs.readFileSync('./public/json/data.json');
  const films = JSON.parse(filmsJSON);
  const filmSet = new Set();
  const match = (req.params.actor || '').toLowerCase();

  // find all films with this actor
  films.movies.forEach((val) => {
    if(val.abridged_cast) {
      val.abridged_cast.forEach((value) => {
        if (value.name.toLowerCase().includes(match)) {
          const url = val.alternate_ids && val.alternate_ids.imdb ? val.alternate_ids.imdb : 0;
          filmSet.add({ title: val.title, link: url });
        }
      });
    }
  });

  // If nothing: error, else: sort by name and send
  if (filmSet.size === 0) {
    console.log('Nothing found');
    res.status(400).send('Nothing found');
  } else {
    const filmArr = Array.from(filmSet).sort((a,b) => {
      return a.name > b.name;
    });
    console.log(`Actors sent: ${filmArr.length}`);
    res.status(200).json(filmArr);
  }
});

module.exports = router;
