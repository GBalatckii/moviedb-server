const express = require('express');
const router = express.Router();
const fs = require('fs');

/* POST add new film */
router.post('/', function(req, res) {
  const filmsJSON = fs.readFileSync('./public/json/data.json');
  const films = JSON.parse(filmsJSON);
  let foundMatch = false;

  // check if already exist (title + year)
  films.movies.forEach((val) => {
    if (val.title === req.body.title && val.year === req.body.year) {
      foundMatch = true;
    }
  });

  // If already exists: error, else: save to file and send same film back
  if (foundMatch) {
    console.log('Nothing found');
    res.status(400).send('Already exist');
  } else {
    films.movies.push(req.body);

    fs.writeFile('./public/json/data.json', JSON.stringify(films, null, 2), (err) => {
      if (err) throw err;
    });

    console.log(`Film saved: ${req.body.title}`);
    res.status(200).json(req.body);
  }
});

module.exports = router;
