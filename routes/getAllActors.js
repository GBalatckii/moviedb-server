const express = require('express');
const router = express.Router();
const fs = require('fs');

/* GET all actors */
router.get('/(:actor)?', function(req, res) {
  const filmsJSON = fs.readFileSync('./public/json/data.json');
  const films = JSON.parse(filmsJSON);
  const actorSet = new Set();
  const match = (req.params.actor || '').toLowerCase();

  // go throught and save all matched values
  films.movies.forEach((val) => {
    val.abridged_cast.forEach((value) => {
      if ((value.name).toLowerCase().includes(match)) { actorSet.add(value.name); }
    });
  });

  // If nothing: error, else: sort by name and send
  if (actorSet.size === 0) {
    console.log('Nothing found');
    res.status(400).send('Nothing found');
  } else {
    const actorArr = Array.from(actorSet);
    console.log(`Actors sent: ${actorArr.length}`);
    res.status(200).json(actorArr);
  }
});

module.exports = router;
