const express = require('express');
const router = express.Router();
const fs = require('fs');

/* GET all films */
router.get('/', function(req, res) {

  fs.readFile('./public/json/data.json', (err, data) => {
    const filmData = JSON.parse(data);

    // sort films with undefined values
    filmData.movies = filmData.movies.filter(val => (
      typeof val === 'object' &&
      val.title &&
      val.year &&
      val.ratings &&
      val.ratings.critics_score && val.ratings.critics_score > 0 &&
      val.ratings.audience_score && val.ratings.audience_score > 0 &&
      val.abridged_cast && val.abridged_cast.length >= 5 &&
      val.alternate_ids && val.alternate_ids.imdb
    ));

    console.log(`Films sent: ${filmData.movies.length}`);
    res.status(200).send(JSON.stringify(filmData));
  });
});

module.exports = router;
